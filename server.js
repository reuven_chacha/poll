var http = require('http'),
	db = require('./db'),
	app = require('./app'),
	pollResultSchema  = require('./schema/poll-result');

http.createServer(app).listen(3000, function(){
 	console.log('Express server listening on port ' +3000);


	var Person = db.model('Person',  { name: String, lastName: String });  	
	var person = new Person ({
	 	name : 'Reuven',
	 	lastName : 'Chacha'
	});

	pollResultSchema.find().setOptions({ sort : 'date'}).exec( function (err, all) {
			if (err) console.log(err);
			console.log(all);
		});
	
});