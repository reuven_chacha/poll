var myApp = angular.module('allResults', ['ngCookies']);


myApp.controller('mainController', ['$scope', '$http', '$cookieStore', function($scope, $http, $cookieStore) {

	$scope.all = {};
	$scope.allArr = [];
	$scope.desc = "";
	$scope.author = "";
	var getQueryParam = function(param) {
		var found;
		window.location.search.substr(1).split("&").forEach(function(item) {
			if (param ==  item.split("=")[0]) {
				found = item.split("=")[1];
			}
		});
		return found;
	};


	$(document).ready(function() {
		$("#submit").hide();

	})

	var add = function (obj) {
		
		if ($scope.all[obj._id] === undefined) {
			$scope.all[obj._id] = $scope.allArr.length;
			$scope.allArr.push(obj);
		}
		else {
			var index = $scope.all[obj._id];
			if ($scope.allArr[index].desc != obj.desc) $scope.allArr[index].desc = obj.desc;
			$scope.allArr[index].counter = obj.counter;	
		}
	}

	var updateInfo = function() {
		
		$http.get("/allJSON").
		success(function(data) {
			for(var ind in data)
				add(data[ind]);
			$http.get('admin/' + getQueryParam("pass")).success(function () {
				$scope.finished = true;
			}).error(function() {});
				
		}).error(function() {});		
	}
	

	updateInfo();
	

	$("form").on("submit", function(event) {
		event.preventDefault();
		
		var data = {};
		$("#submitNewForm").serializeArray().map(function(x){data[x.name] = x.value;});
		$.ajax( {
			type: "POST",
			url: "submit/do",
			processData: false,
			contentType: 'application/json',
			data: JSON.stringify(data),
			statusCode: 
			{ 
				200 : function(res) {
					data.counter = 0;
					data._id = res._id;
					$scope.$apply(updateInfo());
					if ($scope.shown) {
						$scope.shown = false;
						$("#submit").hide(300);
					}
				}
			}
		});
		return false;
	});


	$("#add").click(function(){
		if (!$scope.shown) {
			$scope.shown = true;
			$("#submit").show(300);
			$("#add").toggleClass("active", true);
		}
		else {
			$scope.shown = false;
			$("#submit").hide(300);
			$("#add").toggleClass("active", false);
		}
	});



	$scope.delete = function(id) {
		$http.delete("/delete/" + id).success(function() {
			$scope.allArr[$scope.all[id]] = undefined;
			delete $scope.all[id];

			updateInfo();
		});
	}


	$scope.increase = function(id) {
		if ($cookieStore.get(id) === undefined)
			$cookieStore.put(id, 0);
		if ($cookieStore.get(id) < 1) {
			$http.get("/inc/" + id).
			success(function(data) {
				$cookieStore.put(id, $cookieStore.get(id) + 1);
				updateInfo();
			}).error(function() {});		
		}
		else {
			alert('כבר הצבעת בעד הצעה זו');
		}
	}

	$scope.decrease = function(id) {
		
		if ($cookieStore.get(id) === undefined) 
			$cookieStore.put(id, 0);
		if ($cookieStore.get(id) > -1) {
			$http.get("/dec/" + id).
			success(function(data) {
				$cookieStore.put(id, $cookieStore.get(id) - 1);
				updateInfo();
			}).error(function() {});			
		}
		else {
			alert('כבר הצבעת נגד הצעה זו');
		}
		
	}
}]);