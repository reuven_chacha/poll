var pollResult = function () {
	
	this.data = {
		desc :  null,
		author : null,
		date : null,
		counter : null
	};

	this.fill = function (info) {
		for(var prop in this.data) {
			if(this.data[prop] !== 'undefined') {
				this.data[prop] = info[prop];
			}
		}
		if (this.data.counter === undefined) 		
			this.data.counter = 0;

		this.data.date = new Date();
	};

	this.increaseCount = function() {
		this.data.counter += 1;
	}

	this.decreaseCount = function () {
		this.data.counter -= 1;
	}

	this.getInfo = function() {
		return this.data;
	}
};

module.exports = function (info) {
	var instance = new pollResult();
	instance.fill(info);
	return instance;
};