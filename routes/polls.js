
/*
 * GET home page.
 */

 var pollResultSchema  = require('../schema/poll-result');
 var pollResult = require('../modules/pollResult');

 module.exports = function () {

 	var functions = {};
 	var errorJSON = { 
 		code : 404, 
 		status : 'error'
 	};

 	functions.submit = function (req, res) {
 		var result = req.body;
 		console.log(result.desc);
 		if (result.desc !== undefined && result.desc.length > 10) {
 			var pollRes = new pollResultSchema(pollResult(result).getInfo());
 			console.log(JSON.stringify(result));			
 			pollRes.save(function (err, saved) {
 				if (err) res.status(errorJSON.code).json({ error: err});
 				else res.json(saved);
 			});
 		}
 	};

 
 	functions.all = function(req, res) {
 		pollResultSchema.find().setOptions({ sort : 'counter'}).exec( function (err, all) {
 			if (err) res.json(err);
 			else {
 				res.render('all', {});
 			}
 		});
 	};

 	functions.getAllJSON = function(req, res) {
 		pollResultSchema.find().setOptions({ sort : { 'counter' : -1} }).exec( function (err, all) {
 			if (err) res.json(err);
 			else 
 				res.json(all);
 		});
 	};


 	functions.increaseVote = function(req, res) {
 		if (!req.params['id']) res.json( {"status" : "error"});

 		pollResultSchema.findOne({ "_id" : req.params['id'] }).exec(function (err, doc) {
 			if (!err) {
 				doc.counter = doc.counter + 1;
 				doc.save();
 				res.json({"status" : "success"});
 			}
 		});
 	};

 	functions.decreaseVote = function(req, res) {

 		if (!req.params['id']) res.json( {"status" : "error"});

 		pollResultSchema.findOne({ "_id" : req.params['id'] } ).exec(function (err, doc) {
 			if (!err) {
 				doc.counter= doc.counter- 1;
 				doc.save();
 				res.json({"status" : "success"});
 			}
 		});
 	};

 	functions.admin = function(req, res) {

 		if (!req.params['pass']) res.json( {"status" : "error"});
 		if (req.params['pass'] == "chacha")
 			res.json({status: "success"});
 		else res.status(500).send("Not Allowed");
 	}

 	functions.delete = function (req, res) {
 	
 		if (!req.params['id']) res.json( {"status" : "error"});

 		pollResultSchema.remove({ "_id" : req.params['id'] } ).exec(function (err) {
 			if (!err) {
 				res.json({"status" : "success"});
 			}
 			else {
 				res.json({"status" : "success"});	
 			}
 		});

 	};


 	return functions;
 };
