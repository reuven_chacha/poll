var mongoose = require('mongoose');

module.exports = mongoose.model('pollResult', {
	    desc :   String,
	    author : String,
	    date : Date,
	    counter : Number
});